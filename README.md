﻿# Apache Shiro

#### 介绍
Apache Shiro是Java的一个安全框架

#### 目录
###### 第一章 Shiro简介
###### 第二章 身份验证
###### 第三章 授权
###### 第四章 INI配置
###### 第五章 编码/加密
###### 第六章 Realm及相关对象
###### 第七章 与Web集成
###### 第八章 过滤器机制
###### 第九章 JSP标签
###### 第十章 会话管理
###### 第十一章 缓存机制
###### 第十二章 与Spring集成
###### 第十三章 RememberMe
###### 第十四章 SSL
###### 第十五章 单点登录
###### 第十六章 综合实例
###### 第十七章 OAuth2
###### 第十八章 并发登录人数控制
###### 第十九章 动态URL权限控制
###### 第二十章 无状态Web应用集成
###### 第二十一章 授予身份及切换身份
###### 第二十二章 集成验证码
###### 第二十三章 多项目集中权限管理及分布式会话
###### 第二十四章 在线会话管理

#### 参考资料
1.  https://www.iteye.com/blog/jinnianshilongnian-2018398
2.  https://www.sojson.com/shiro#so730728009
3.  xxxx

#### 使用说明

1.  xxxx
2.  xxxx
3.  xxxx

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 码云特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  码云官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解码云上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是码云最有价值开源项目，是码云综合评定出的优秀开源项目
5.  码云官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  码云封面人物是一档用来展示码云会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
