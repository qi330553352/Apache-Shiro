package com.example;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ShiroAuthorizeChapter3Application {

	public static void main(String[] args) {
		SpringApplication.run(ShiroAuthorizeChapter3Application.class, args);
	}

}
