/**
 * Copying (c) Qixin Technoligies Co.,Ltd.2019-2029.all rights reserved.
 */

package com.example.controller;

import com.example.WSFAspect;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.Map;

/**
 * 创  建   时  间： 2020/7/28
 * 版           本: V1.0
 * 作           者: qixin
 * 版  权   所  有: 版权所有(C)2019-2029
 * 公           司: 深圳市七智星科技有限公司
 */
@RestController
public class ExampleController {
    private Logger log = LoggerFactory.getLogger(ExampleController.class);

    @GetMapping("/test")
    public Map<String,Object> test(@RequestParam("name")String name){
        log.info("name:"+name);
        Map<String,Object> map = new HashMap<>();
        map.put("string","abc");
        map.put("Integer",123);
        return map;
    }

    @PostMapping("/test")
    public Map<String,Object> test1(String name,Integer age){
        log.info("name:"+name +" age:"+age);
        Map<String,Object> map = new HashMap<>();
        map.put("string","abc");
        map.put("Integer",123);
        return map;
    }
}
