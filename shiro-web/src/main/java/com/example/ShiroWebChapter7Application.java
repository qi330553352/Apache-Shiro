package com.example;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ShiroWebChapter7Application {

	public static void main(String[] args) {
		SpringApplication.run(ShiroWebChapter7Application.class, args);
	}

}
