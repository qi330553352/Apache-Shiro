/**
 * Copying (c) Yurian Technoligies Co.,Ltd.2019-2029.all rights reserved.
 */

package com.example.controller;

import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.AuthenticationException;
import org.apache.shiro.authc.IncorrectCredentialsException;
import org.apache.shiro.authc.UnknownAccountException;
import org.apache.shiro.authc.UsernamePasswordToken;
import org.apache.shiro.subject.Subject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * 创  建   时  间： 2020/4/8
 * 版           本: V1.0
 * 作           者: qixin
 * 版  权   所  有: 版权所有(C)2019-2029
 * 公           司: 悠喃食品(深圳)有限公司
 */
@Controller
public class JspController {
    private Logger log = LoggerFactory.getLogger(this.getClass());
    @RequestMapping(value = "/index")
    public String index(Model model) {
        model.addAttribute("message", "zlj");

        return "index";
    }

    @RequestMapping(value = "/loginUI")
    public String loginUI(Model model) {
        model.addAttribute("message", "zlj");

        return "login";
    }

    @RequestMapping(value = "/login",method = RequestMethod.POST)
    public String login(Model model,String username,String password) {
        log.info("username:"+username+" password:"+password);
        Subject subject = SecurityUtils.getSubject();
        UsernamePasswordToken token = new UsernamePasswordToken(username, password);
        token.setRememberMe(true);
        String error = null;
        try {
            subject.login(token);
        } catch (UnknownAccountException e) {
            error = "用户名/密码错误";
        } catch (IncorrectCredentialsException e) {
            error = "用户名/密码错误";
        } catch (AuthenticationException e) {
            //其他错误，比如锁定，如果想单独处理请单独catch处理
            error = "其他错误：" + e.getMessage();

        }
        if(StringUtils.isEmpty(error)){
            return error;
        }else{
            return "loginSuccess";
        }
    }
}
