package com.example;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ShiroIniConfigChapter4Application {

	public static void main(String[] args) {
		SpringApplication.run(ShiroIniConfigChapter4Application.class, args);
	}

}
